const express = require("express");
const app = express();

app.listen(3000, () => {
  console.log("Start server at port 3000.");
});

var mysql = require("mysql");

var http = require("http");
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "12345678",
  database: "exam",
});

app.use(express.static(__dirname + "/img"));

con.connect(function (err) {
  if (err) throw err;
  con.query(
    "SELECT employee.id ,  employee.firstname , employee.lastname , job.job_name   FROM employee Join job ON employee.job_id=job.id ",
    function (err, result, fields) {
      if (err) throw err;
      console.log(result);

      app.set("view engine", "ejs");

      app.get("/employee", function (req, res) {
        res.render("homepage", {
          detail: "Table",
          data: result,
        });
      });
    }
  );

  con.query("SELECT id,job_name FROM job", function (err, result, fields) {
    if (err) throw err;
    console.log(result);

    app.set("view engine", "ejs");

    app.get("/job-no-one/:id?", function (req, res) {
      console.log(req.params);
      let returnData = result;
      returnData = req.params.id
        ? returnData.filter((c) => c.id == +req.params.id)
        : returnData;
      res.render("job", {
        detail: "JOB",
        data: returnData,
        id: +req.id,
      });
    });
  });
});
