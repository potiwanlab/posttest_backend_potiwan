const express = require("express");
const { google } = require("googleapis");
const keys = require("./testapi-334008-b6fb9081c0d1.json");

//initialize express
const app = express();
app.use(express.urlencoded({ extended: true }));

//set up template engine to render html files
app.set("view engine", "ejs");
app.engine("html", require("ejs").renderFile);

const auth = new google.auth.GoogleAuth({
  keyFile: "testapi-334008-b6fb9081c0d1.json", //the key file
  //url to spreadsheets API
  scopes: "https://www.googleapis.com/auth/spreadsheets",
});

auth.getClient().then((authClientObject) => {
  //Google sheets instance
  const googleSheetsInstance = google.sheets({
    version: "v4",
    auth: authClientObject,
  });

  // spreadsheet id
  const spreadsheetId = "1AFni2vxsK2xrXc2Pyuop2Ga6auXWtixzyJFzKhsJD6w";

  //Read from the spreadsheet
  googleSheetsInstance.spreadsheets.values
    .get({
      auth, //auth object
      spreadsheetId, // spreadsheet id
      range: "Sheet1!A1:E7", //range of cells to read from.
    })
    .then((data) => {
      console.log(data.data);
      const allData = data.data.values;
      app.get("/", async (request, response) => {
        response.render("index", { allData: allData });
      });
    });
});

const PORT = 3000;

//start server
const server = app.listen(PORT, () => {
  console.log(`Server started on port localhost:${PORT}`);
});
