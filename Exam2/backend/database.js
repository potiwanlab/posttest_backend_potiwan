const users = [
  {
    userName:"john",
    email: "john@gmail.com",
    password: "random",
  },
  {
    userName:"jack",
    email: "jack@gmail.com",
    password: "JackPassword",
  },
];

let onLogin = 
  {
    userName:"john",
    email: "john@gmail.com",

  }
;

const publicPosts = [
  {
    title: "Post 1",
    content: "Post 1 is free",
  },
  {
    title: "Post 2",
    content: "Post 2 is free",
  },
];

const privatePosts = [
  {
    title: "Post 3",
    content: "Post 3 is private",
  },
];

module.exports = { users, publicPosts, privatePosts,onLogin };
