const router = require("express").Router();
const { publicPosts, privatePosts, onLogin } = require("../database");
const authToken = require("../middleware/authenticateToken");

router.get("/public", (req, res) => {
  res.json(publicPosts);
});

router.get("/profile", authToken, (req, res) => {
  let data = {
    onLogin,
    privatePosts,
  };
  res.json(data);
});

module.exports = router;
