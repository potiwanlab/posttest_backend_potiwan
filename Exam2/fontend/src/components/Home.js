import React, { useState, useEffect } from "react";
import PostService from "../services/post.service";
import AuthService from "../services/auth.service";

const Home = () => {
  const [posts, setPosts] = useState([]);
  const [currentUser, setCurrentUser] = useState(undefined);
  const [loginSuccess, setLoginSuccess] = useState([]);

  useEffect(() => {

    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
      setLoginSuccess("Login สำเร็จ")
    }


    PostService.getAllPublicPosts().then(
      (response) => {
        setPosts(response.data);
      },
      (error) => {
        console.log(error);
      }
    );
  }, []);

  return (
    <div>
      {currentUser && <h1>{loginSuccess}</h1>}
      <h3>
        {posts.map((post, index) => (
          <div key={index}>{post.content}</div>
        ))}
      </h3>
    </div>
  );
};

export default Home;
